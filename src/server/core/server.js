import convert from 'koa-convert';
import cors from '@koa/cors';
import bodyParser from 'koa-body';
import helmet from 'koa-helmet';
import config from 'config';
import serve from 'koa-static';
import mount from 'koa-mount';
import log4js from 'koa-log4';

import {
  routerControl,
  ioControl,
  graphControl
} from '../app';

import { catchErr, statusMessage } from './error';

import nuxtConfig from './nuxt';

export default (app) => {
  app.keys = config.get('secret');
  app.proxy = true;

  app.use(mount('/public', serve(config.get('paths.static'))));

  app.use(convert.compose(
    catchErr,
    cors(),
    bodyParser({
      multipart: true,
      formLimit: '200mb'
    }),
    helmet(),
    statusMessage,
    log4js.koaLogger(log4js.getLogger('http'), { level: 'auto' })
  ));
  routerControl(app);
  ioControl(app);
  graphControl(app);

  if (config.get('nuxtBuild')) {
    nuxtConfig(app);
  }
};
